package com.parkinglot;

import com.parkinglot.exception.IsWrongTicketException;
import com.parkinglot.exception.NoAvailablePositionException;

import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy {
    private final List<ParkingLot> parkingLotList;

    public SmartParkingBoy(List<ParkingLot> parkingLotList) {
        this.parkingLotList = parkingLotList;
    }
    public Ticket park(Car car) {
        return parkingLotList.stream()
                .max(Comparator.comparingInt(ParkingLot::queryAvailableParkingSpaces))
                .orElseThrow(() -> new NoAvailablePositionException("No available position"))
                .park(car);
    }
    public Car fetch(Ticket receiveTicket) {
        return parkingLotList.stream()
                .filter(parkingLot -> parkingLot.isVaildTicket(receiveTicket))
                .findFirst()
                .orElseThrow(() -> new IsWrongTicketException("Unrecognized parking ticket"))
                .fetch(receiveTicket);
    }
}
