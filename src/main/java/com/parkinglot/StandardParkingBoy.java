package com.parkinglot;

import com.parkinglot.exception.IsWrongTicketException;
import com.parkinglot.exception.NoAvailablePositionException;

import java.util.List;

public class StandardParkingBoy {
    private final List<ParkingLot> parkingLotList;

    public StandardParkingBoy(List<ParkingLot> parkingLotList) {
        this.parkingLotList = parkingLotList;
    }


    public Ticket park(Car car) {
        return parkingLotList.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .findFirst()
                .orElseThrow(() -> new NoAvailablePositionException("No available position"))
                .park(car);
    }

    public Car fetch(Ticket receiveTicket) {
        return parkingLotList.stream()
                .filter(parkingLot -> parkingLot.isVaildTicket(receiveTicket))
                .findFirst()
                .orElseThrow(() -> new IsWrongTicketException("Unrecognized parking ticket"))
                .fetch(receiveTicket);
    }
}
