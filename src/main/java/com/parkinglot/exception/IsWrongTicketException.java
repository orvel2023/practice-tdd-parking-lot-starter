package com.parkinglot.exception;

public class IsWrongTicketException extends  RuntimeException{
    public IsWrongTicketException(String message) {
        super(message);
    }
}
