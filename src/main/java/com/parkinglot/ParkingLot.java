package com.parkinglot;

import com.parkinglot.exception.IsWrongTicketException;
import com.parkinglot.exception.NoAvailablePositionException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private final Map<Ticket, Car> ticketCarMap;
    private int packingLotCapacity = 10;

    public int getPackingLotCapacity() {
        return packingLotCapacity;
    }

    public ParkingLot() {
        this.ticketCarMap = new HashMap<>(packingLotCapacity);
    }
    public ParkingLot(int packingLotCapacity) {
        this.packingLotCapacity = packingLotCapacity;
        this.ticketCarMap = new HashMap<>(packingLotCapacity);
    }

    public Ticket park(Car car) {
        if (this.ticketCarMap.size() == packingLotCapacity) {
            throw new NoAvailablePositionException("No available position");
        }
        Ticket ticket = new Ticket();
        this.ticketCarMap.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket receiveTicket)  {
        if (this.ticketCarMap.containsKey(receiveTicket)) {
            Car getTheCar = this.ticketCarMap.get(receiveTicket);
            this.ticketCarMap.remove(receiveTicket);
            return getTheCar;
        }
        throw new IsWrongTicketException("Unrecognized parking ticket");
    }
    public boolean isFull() {
        return ticketCarMap.size() >= packingLotCapacity;
    }
    public boolean isVaildTicket(Ticket ticket) {
        return ticketCarMap.containsKey(ticket);
    }

    public int queryAvailableParkingSpaces() {
        return packingLotCapacity - ticketCarMap.size();
    }
}
