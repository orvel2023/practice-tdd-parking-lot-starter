package com.parkinglot;

import com.parkinglot.exception.IsWrongTicketException;
import com.parkinglot.exception.NoAvailablePositionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLogTest {
    @Test
    void should_return_a_ticket_when_park_given_a_car() {
        //Given
        Car car = new Car();
        ParkingLot packingLot = new ParkingLot();

        //When
        Ticket receiveTicket = packingLot.park(car);

        //Then
        Assertions.assertNotNull(receiveTicket);
    }

    @Test
    void should_return_a_different_ticket_when_park_given_two_car() {
        //Given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot packingLot = new ParkingLot();

        //When
        Ticket receiveTicket1 = packingLot.park(car1);
        Ticket receiveTicket2 = packingLot.park(car2);

        //Then
        Assertions.assertNotEquals(receiveTicket1, receiveTicket2);
    }

    @Test
    void should_throws_error_message_when_park_in_packinglot_full_given_car() {
        //Given
        Car car = new Car();
        ParkingLot packingLot = new ParkingLot();
        for (int i = 0; i < 10; i++) {
            packingLot.park(new Car());
        }

        //When

        //Then
        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> packingLot.park(car));
        Assertions.assertEquals(exception.getMessage(), "No available position");
    }

    @Test
    void should_return_correct_car_when_fetch_given_a_correct_ticket() {
        //Given
        Car car = new Car();
        ParkingLot packingLot = new ParkingLot(10);
        Ticket receiveTicket = packingLot.park(car);
        //When
        Car getTheCar = packingLot.fetch(receiveTicket);

        //Then
        Assertions.assertEquals(car, getTheCar);
    }

    @Test
    void should_throws_error_message_when_fetch_given_a_used_ticket() throws IsWrongTicketException {
        //Given
        Car car = new Car();
        ParkingLot packingLot = new ParkingLot();
        Ticket receiveTicket = packingLot.park(car);
        packingLot.fetch(receiveTicket);
        //When

        //Then
        Exception exception = Assertions.assertThrows(IsWrongTicketException.class, () -> packingLot.fetch(receiveTicket));
        Assertions.assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throws_error_message_when_fetch_given_a_error_ticket() throws IsWrongTicketException {
        //Given
        ParkingLot packingLot = new ParkingLot();
        Ticket ticket = new Ticket();
        //When

        //Then
        Exception exception = Assertions.assertThrows(IsWrongTicketException.class, () -> packingLot.fetch(ticket));
        Assertions.assertEquals(exception.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_return_1_when_queryAvailableParkingSpaces_given_parkinglot_of_capacity_is_2_parked_1() {
        //Given
        ParkingLot packingLot = new ParkingLot(2);
        packingLot.park(new Car());

        //When
        int availableParkingSpaces = packingLot.queryAvailableParkingSpaces();

        //Then
        Assertions.assertEquals(1, availableParkingSpaces);
    }
}
