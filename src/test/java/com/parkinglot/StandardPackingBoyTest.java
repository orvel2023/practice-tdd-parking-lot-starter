package com.parkinglot;

import com.parkinglot.exception.IsWrongTicketException;
import com.parkinglot.exception.NoAvailablePositionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class StandardPackingBoyTest {
    @Test
    void should_return_a_ticket_when_park_given_two_parking_lot_a_car() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(5));
        parkingLotList.add(new ParkingLot(6));
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);

        //When
        Ticket receiveTicket = standardParkingBoy.park(new Car());

        //Then
        Assertions.assertNotNull(receiveTicket);
    }

    @Test
    void should_return_a_different_ticket_when_park_given_two_car_and_two_parkinglot() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);

        //When
        Ticket receiveTicket1 = standardParkingBoy.park(new Car());
        Ticket receiveTicket2 = standardParkingBoy.park(new Car());

        //Then
        Assertions.assertNotEquals(receiveTicket1, receiveTicket2);
    }

    @Test
    void should_throws_error_message_when_fetch_given_a_wrong_ticket() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(1));
        parkingLotList.add(new ParkingLot(1));
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        standardParkingBoy.park(new Car());
        Ticket ticket = new Ticket();
        //When

        //Then
        Exception exception = Assertions.assertThrows(IsWrongTicketException.class,() -> standardParkingBoy.fetch(ticket));
        Assertions.assertEquals(exception.getMessage(),"Unrecognized parking ticket");
    }
    @Test
    void should_throws_error_message_when_fetch_given_a_used_ticket() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(1));
        parkingLotList.add(new ParkingLot(1));
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        Ticket receiveTicket = standardParkingBoy.park(new Car());
        standardParkingBoy.fetch(receiveTicket);
        //When

        //Then
        Exception exception = Assertions.assertThrows(IsWrongTicketException.class,() -> standardParkingBoy.fetch(receiveTicket));
        Assertions.assertEquals(exception.getMessage(),"Unrecognized parking ticket");
    }
    @Test
    void should_throws_error_message_when_park_given_two_full_packing_and_a_car() {
        //Given
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        //When

        //Then

        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(car));
        Assertions.assertEquals(exception.getMessage(), "No available position");
    }
    @Test
    void should_return_a_ticket_when_park_given_a_full_packintlot_and_a_empty_packinglot_two_car() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot fullParkingLot = new ParkingLot(1);
        fullParkingLot.park(new Car());
        parkingLotList.add(new ParkingLot(10));
        StandardParkingBoy packingBoy = new StandardParkingBoy(parkingLotList);

        //When
        Ticket receiveTicket = packingBoy.park(new Car());

        //Then
        Assertions.assertNotNull(receiveTicket);
    }
    @Test
    void should_return_a_correct_car_when_fetch_given_two_car_in_two_parkinglot() {
        //Given
        Car car1 = new Car();
        Car car2 = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        Ticket ticket1 = parkingLot1.park(car1);
        Ticket ticket2 = parkingLot2.park(car2);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        //When
        Car getTheCar1 = standardParkingBoy.fetch(ticket1);
        Car getTheCar2 = standardParkingBoy.fetch(ticket2);

        //Then
        Assertions.assertEquals(car1, getTheCar1);
        Assertions.assertEquals(car2, getTheCar2);
    }

}

