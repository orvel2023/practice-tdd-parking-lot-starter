package com.parkinglot;

import com.parkinglot.exception.IsWrongTicketException;
import com.parkinglot.exception.NoAvailablePositionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SuperParkingBoyTest {
    @Test
    void should_park_in_larger_available_position_rate_parking_lot_when_park_given_two_parkiinglot_and_a_car() {

        //Given
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(7);
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());
        parkingLot2.park(new Car());
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLotList);

        //When
        Ticket receiveTicket = superParkingBoy.park(car);

        //Then
        Assertions.assertEquals(car, parkingLot1.fetch(receiveTicket));
    }

    @Test
    void should_throws_error_message_when_fetch_given_a_used_ticket() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(1));
        parkingLotList.add(new ParkingLot(1));
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLotList);
        Ticket receiveTicket = superParkingBoy.park(new Car());
        superParkingBoy.fetch(receiveTicket);
        //When

        //Then
        Exception exception = Assertions.assertThrows(IsWrongTicketException.class,() -> superParkingBoy.fetch(receiveTicket));
        Assertions.assertEquals(exception.getMessage(),"Unrecognized parking ticket");
    }

    @Test
    void should_throws_error_message_when_fetch_given_a_wrong_ticket() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(1));
        parkingLotList.add(new ParkingLot(1));
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLotList);
        superParkingBoy.park(new Car());
        Ticket ticket = new Ticket();
        //When

        //Then
        Exception exception = Assertions.assertThrows(IsWrongTicketException.class,() -> superParkingBoy.fetch(ticket));
        Assertions.assertEquals(exception.getMessage(),"Unrecognized parking ticket");
    }

    @Test
    void should_throws_error_message_when_park_given_two_full_packing_and_a_car() {
        //Given
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLotList);
        //When

        //Then

        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> superParkingBoy.park(car));
        Assertions.assertEquals(exception.getMessage(), "No available position");
    }

    @Test
    void should_return_correct_car_when_fetch_given_two_correct_ticket_and_two_parking_lot() {
        //Given
        Car car1 = new Car();
        Car car2 = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(1));
        parkingLotList.add(new ParkingLot(1));
        SuperParkingBoy  superParkingBoy= new SuperParkingBoy(parkingLotList);
        Ticket receiveTicket1 = superParkingBoy.park(car1);
        Ticket receiveTicket2 = superParkingBoy.park(car2);
        //When
        Car getTheCar1 = superParkingBoy.fetch(receiveTicket1);
        Car getTheCar2 = superParkingBoy.fetch(receiveTicket2);
        //Then
        Assertions.assertEquals(car1, getTheCar1);
        Assertions.assertEquals(car2, getTheCar2);
    }

    @Test
    void should_return_a_different_ticket_when_park_given_two_car_and_two_parkinglot() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLotList);

        //When
        Ticket receiveTicket1 = superParkingBoy.park(new Car());
        Ticket receiveTicket2 = superParkingBoy.park(new Car());

        //Then
        Assertions.assertNotEquals(receiveTicket1, receiveTicket2);
    }
    @Test
    void should_return_a_ticket_when_park_given_a_full_packintlot_and_a_empty_packinglot_two_car() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot fullParkingLot = new ParkingLot(1);
        fullParkingLot.park(new Car());
        parkingLotList.add(new ParkingLot(10));
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLotList);

        //When
        Ticket receiveTicket = superParkingBoy.park(new Car());

        //Then
        Assertions.assertNotNull(receiveTicket);
    }
}
