package com.parkinglot;

import com.parkinglot.exception.IsWrongTicketException;
import com.parkinglot.exception.NoAvailablePositionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoyTest {
    @Test
    void should_return_a_ticket_when_park_given_two_parkiinglot_and_a_car() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(10));
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLotList);

        //When
        Ticket receiveTicket = smartParkingBoy.park(new Car());

        //Then
        Assertions.assertNotNull(receiveTicket);
    }
    @Test
    void should_return_different_ticket_when_park_given_two_parkiinglot_and_two_car() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLotList);

        //When
        Ticket receiveTicket1 = smartParkingBoy.park(new Car());
        Ticket receiveTicket2 = smartParkingBoy.park(new Car());

        //Then
        Assertions.assertNotEquals(receiveTicket1, receiveTicket2);
    }
    @Test
    void should_return_a_ticket_when_park_given_a_full_packintlot_and_a_empty_packinglot_two_car() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot fullParkingLot = new ParkingLot(1);
        fullParkingLot.park(new Car());
        parkingLotList.add(new ParkingLot(10));
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLotList);

        //When
        Ticket receiveTicket = smartParkingBoy.park(new Car());

        //Then
        Assertions.assertNotNull(receiveTicket);
    }
    @Test
    void should_throws_error_message_when_park_given_two_full_packing_and_a_car() {
        //Given
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLotList);

        //When

        //Then

        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(car));
        Assertions.assertEquals(exception.getMessage(), "No available position");
    }
    @Test
    void should_throws_error_message_when_fetch_given_a_used_ticket() {
        //Given
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(10));
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLotList);
        Ticket receiveTicket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(receiveTicket);
        //When

        //Then
        Exception exception = Assertions.assertThrows(IsWrongTicketException.class,() -> smartParkingBoy.fetch(receiveTicket));
        Assertions.assertEquals(exception.getMessage(),"Unrecognized parking ticket");
    }
    @Test
    void should_throws_error_message_when_fetch_given_a_wrong_ticket() {
        //Given
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(10));
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLotList);
        Ticket ticket = new Ticket();
        //When

        //Then
        Exception exception = Assertions.assertThrows(IsWrongTicketException.class,() -> smartParkingBoy.fetch(ticket));
        Assertions.assertEquals(exception.getMessage(),"Unrecognized parking ticket");
    }
    @Test
    void should_park_in_more_vacant_parking_lot_when_park_given_two_parkiinglot_and_a_car() {
        //Given
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLotList);
        //When
        Ticket receiveTicket = smartParkingBoy.park(car);
        //Then

        Assertions.assertEquals(car, parkingLot2.fetch(receiveTicket));
    }
    @Test
    void should_return_a_correct_car_when_fetch_given_two_car_in_two_parkinglot() {
        //Given
        Car car1 = new Car();
        Car car2 = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        Ticket ticket1 = parkingLot1.park(car1);
        Ticket ticket2 = parkingLot2.park(car2);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLotList);
        //When
        Car getTheCar1 = smartParkingBoy.fetch(ticket1);
        Car getTheCar2 = smartParkingBoy.fetch(ticket2);

        //Then
        Assertions.assertEquals(car1, getTheCar1);
        Assertions.assertEquals(car2, getTheCar2);
    }
}
