# O
  - Code Review : I find that my understanding of design patterns is not deep enough, and I may need to learn more about design patterns in the future and use them in daily development.
  - TDD With OOP : I think today's tutorial will help you put TDD into practice and deepen your understanding of it. By combining OOP programming, the whole process is closer to real development.
  - Practice ： In today's practice, I deeply understand the important role of TDD in AFS. When a business requirement is constantly iterated, it is necessary to ensure the integrity of the function through TEST, and in the process of realizing the new function, other irrelevant content cannot be destroyed. As a result, the tests written previously ensure that newly developed modules do not affect other modules.
  - Other understanding : As a design behavior, TDD can well assist our development, that is, we create test cases according to business requirements, and complete the implementation of the entire functional code through the confirmed input and output according to different inputs, so that the logic in the process of implementation will be clearer.
  

# R
  - More real !
# I
  - I think today's tutorial will help you put TDD into practice and deepen your understanding of it. The biggest significance is that the combination of OOP programming makes the whole process closer to real development.
# D
  - Use TDD for daily development.
  - Deepen study of TDD and learn about other related concepts.
  - Deepen understanding of OOP.
  - Deepen understanding of TDD and OOP.